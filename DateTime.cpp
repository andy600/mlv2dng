#include "DateTime.h"
#include <cstdio>

// x=((y*365) + (y/4) - (y/100) + (y/400)) <==> x= 146097y/400 <==> y = 400x/146097
// http://www.wolframalpha.com/input/?i=x%3D%28%28y*365%29+%2B+%28y%2F4%29+-+%28y%2F100%29+%2B+%28y%2F400%29%29

const uint8_t DateTime::daysInMonths[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

DateTime::DateTime(uint64_t microseconds)
	:microseconds(microseconds)
{}

DateTime::DateTime(uint16_t year, uint16_t dayOfYear, uint16_t hour, uint16_t minute, uint16_t second, uint64_t microsecond)
	:microseconds(microsecond)
{
	microseconds += second * microsecInSec;
	microseconds += minute * microsecInMin;
	microseconds += hour * microsecInHour;
	microseconds += dayOfYear * microsecInDay;
	microseconds += ((146097*year)/400) * microsecInDay;
}

std::string DateTime::toString() const
{
	uint16_t year, month, dayOfMonth, hour, minute, second;
	uint32_t microsecond;
	toValues(year, month, dayOfMonth, hour, minute, second, microsecond);
	char timeString[20];
	sprintf(timeString, "%04u:%02u:%02u %02u:%02u:%02u", year, month, dayOfMonth, hour, minute, second);
	return std::string(timeString);
}

void DateTime::toString(std::string& dateTime, std::string& microsecond) const
{
	uint16_t year, month, dayOfMonth, hour, minute, second;
	uint32_t microseconds;
	toValues(year, month, dayOfMonth, hour, minute, second, microseconds);
	char timeString[20];
	sprintf(timeString, "%04u:%02u:%02u %02u:%02u:%02u", year, month, dayOfMonth, hour, minute, second);
	dateTime = std::string(timeString);

	char subsecTime[7];
	sprintf(subsecTime, "%06i", static_cast<int32_t>(microseconds%1000000));
	microsecond = std::string(subsecTime);
}

void DateTime::toValues(uint16_t& year, uint16_t& dayOfYear, uint16_t& hour, uint16_t& minute, uint16_t& second, uint32_t& microsecond) const
{
	uint64_t remaining = microseconds;
	year = static_cast<uint16_t>((400*(remaining/microsecInDay))/146097);
	remaining -= ((146097*year)/400) * microsecInDay;

	dayOfYear = static_cast<uint16_t>(remaining / microsecInDay);
	remaining -= dayOfYear * microsecInDay;

	hour = static_cast<uint16_t>(remaining / microsecInHour);
	remaining -= hour * microsecInHour;

	minute = static_cast<uint16_t>(remaining / microsecInMin);
	remaining -= minute * microsecInMin;

	second = static_cast<uint16_t>(remaining / microsecInSec);
	remaining -= second * microsecInSec;

	microsecond = static_cast<uint32_t>(remaining);
}

void DateTime::toValues(uint16_t& year, uint16_t& month, uint16_t& dayOfMonth, uint16_t& hour, uint16_t& minute, uint16_t& second, uint32_t& microsecond) const
{
	uint64_t remaining = microseconds;
	year = static_cast<uint16_t>((400*(remaining/microsecInDay))/146097);
	remaining -= ((146097*year)/400) * microsecInDay;

	uint16_t dayOfYear = static_cast<uint16_t>(remaining / microsecInDay);
	remaining -= dayOfYear * microsecInDay;

	hour = static_cast<uint16_t>(remaining / microsecInHour);
	remaining -= hour * microsecInHour;

	minute = static_cast<uint16_t>(remaining / microsecInMin);
	remaining -= minute * microsecInMin;

	second = static_cast<uint16_t>(remaining / microsecInSec);
	remaining -= second * microsecInSec;

	microsecond = static_cast<uint32_t>(remaining);

	month = 0;
	dayOfMonth = 0;

	uint16_t countedDays = 0;
	for(uint16_t i = 0; countedDays < dayOfYear; ++i, ++month)
	{
		dayOfMonth = dayOfYear - countedDays;
		countedDays += daysInMonths[i];
		countedDays += (i==1?(isLeapYear()?1:0):0);
	}
}

DateTime DateTime::operator + (uint64_t microseconds)
{
	return DateTime(this->microseconds + microseconds);
}

DateTime DateTime::operator + (DateTime rhs)
{
	return DateTime(microseconds + rhs.microseconds);
}

DateTime DateTime::operator - (uint64_t microseconds)
{
	return DateTime(this->microseconds - microseconds);
}

DateTime DateTime::operator - (DateTime rhs)
{
	return DateTime(microseconds - rhs.microseconds);
}

bool DateTime::isLeapYear() const
{
	uint16_t year = static_cast<uint16_t>((400*(microseconds/microsecInDay))/146097);
	if (year % 400 == 0)
	{
	   return true;
	}
	else if (year % 100 == 0)
	{
	   return false;
	}
	else if (year % 4 == 0)
	{
	   return true;
	}
	return false;
}
