#ifndef WHITEBALANCEINFO_H
#define WHITEBALANCEINFO_H

#include <cstdint>
#include "InFilestream.h"
#include "Timestamped.h"

class WhiteBalanceInfo : public Timestamped
{
private:
	uint32_t mode;
	uint32_t kelvin;
	uint32_t gainRed;
	uint32_t gainGreen;
	uint32_t gainBlue;
	uint32_t shift;
	uint32_t balance;
public:
	WhiteBalanceInfo(void);
	WhiteBalanceInfo(InFilestream& inFile);

	uint32_t getMode() const;
	uint32_t getKelvin() const;
	uint32_t getRedGain() const;
	uint32_t getGreenGain() const;
	uint32_t getBlueGain() const;
	uint32_t getShift() const;
	uint32_t getBalance() const;
};

#endif