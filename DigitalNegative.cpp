#include "DigitalNegative.h"
#include <fstream>
#include <cstdint>
#include "Buffer.h"

DigitalNegative::DigitalNegative(void)
{}

DigitalNegative::DigitalNegative(std::string fileName)
{
	InFilestream file(fileName);

	if(file.readUInt32() == 0x002a4949)
	{
		std::vector<uint32_t> ifdOffsets;
		ifdOffsets.push_back(file.readUInt32());
		for(uint32_t i = 0; i < ifdOffsets.size(); ++i)
		{
			file.seek(ifdOffsets[i], std::ios_base::beg);
			imageFileDirectories.push_back(ImageFileDirectory(file));
			auto& currentIfd = imageFileDirectories.back();
			auto sizes = currentIfd.dataSizes();
			auto newIfds = currentIfd.getEntryDataAsUint32Vector(Tag::SubIFDs);
			if(newIfds.size() > 0)
			{
				ifdOffsets.insert(ifdOffsets.end(), newIfds.begin(), newIfds.end());
			}
			if(currentIfd.hasIfdTag(Tag::ExifIFD))
			{
				ifdOffsets.push_back(currentIfd.getEntry(Tag::ExifIFD).getOffset());
			}
		}
	}
}

void DigitalNegative::addIfd(ImageFileDirectory ifd)
{
	imageFileDirectories.push_back(ifd);
}

std::vector<uint32_t> DigitalNegative::getIfdOffsets() const
{
	std::vector<uint32_t> offsets;
	uint32_t offset = 8; // FourCC + start of first IFD

	for(auto& ifd : imageFileDirectories)
	{
		offset += ifd.size();
		offset += ifd.dataSize();
		offsets.push_back(offset);
	}

	uint32_t& endOfHeader = offsets.back();
	endOfHeader = (endOfHeader / 512 + 1) * 512;
	return offsets;
}

void DigitalNegative::writeHeader(OutFilestream& outFile)
{
	// write header
	outFile.writeValue(0x002a4949,sizeof(uint32_t)); // FourCC
	outFile.writeValue(8,sizeof(uint32_t)); // start of first IFD

	// point offset to position after writing header
	uint64_t offset = outFile.currentPosition();

	// write ifds
	for(auto& ifd : imageFileDirectories)
	{
		// update offset to point to empty area after ifd, for writing ifd data
		offset += ifd.size();
		// write ifd and ifd data using the offset. Offset is updated while writing.
		ifd.write(outFile, offset);
		// seek to the new offset before writing next ifd
		outFile.seek(offset, std::ios_base::beg);
	}

	uint64_t zeroTail = getIfdOffsets().back() - outFile.currentPosition();

	outFile.writeZeros(zeroTail);
}
