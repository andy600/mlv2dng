#ifndef RAWINFO_H
#define RAWINFO_H

#include <cstdint>
#include "InFilestream.h"
#include "OutFilestream.h"
#include "Timestamped.h"

class RawInfo : public Timestamped
{
public:
	uint16_t	configuredWidth; //		1920	Configured video resolution, may differ from payload resolution
	uint16_t	configuredHeight; //	1080	Configured video resolution, may differ from payload resolution

	uint32_t	apiVersion;
	uint32_t	imageData; // never used

	uint32_t	height;
	uint32_t	width;
	uint32_t	pitch;

	uint32_t	frameSize;
	uint32_t	bitsPerPixel;

	uint32_t	blackLevel;
	uint32_t	whiteLevel;

	union                       // DNG JPEG info
    {
        struct
        {
            uint32_t x, y;           // DNG JPEG top left corner
            uint32_t width, height;  // DNG JPEG size
        } jpeg;
        struct
        {
            uint32_t origin[2];
            uint32_t size[2];
        } crop;
    };
    union                       // DNG active sensor area (Y1, X1, Y2, X2)
    {
        struct
        {
            uint32_t y1, x1, y2, x2;
        } activeArea;
        uint32_t dngActiveArea[4];
    };

	uint32_t	exposureBias[2];
	uint32_t	cfaPattern;
	uint32_t	calibrationIlluminant;

	uint32_t	colorMatrix[18];

	uint32_t	dynamicRange;


public:
	RawInfo(void);
	RawInfo(InFilestream& inFile);
	void write(OutFilestream& outfile);
};

#endif