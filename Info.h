#ifndef INFO_H
#define INFO_H

#include <cstdint>
#include "InFilestream.h"
#include "Timestamped.h"

class Info : public Timestamped
{
private:
	std::string infoString;
public:
	Info(void);
	Info(InFilestream& inFile, uint32_t length);

	const std::string& getInfoString() const;
};

#endif